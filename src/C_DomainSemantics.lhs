> {-# OPTIONS_GHC -Wno-name-shadowing -Wno-noncanonical-monad-instances #-}
> module C_DomainSemantics where

> import A_OperationalSemantics (Name, Term(..), Constant(..))
> import qualified A_OperationalSemantics as A
> import qualified B_DenotationalSemantics as B
> import Control.Monad (liftM, ap)

> omega :: Term
> omega = Fun "x" (App (Var "x") (Var "x"))

> omega2 :: Term
> omega2 = App omega omega

> infiniteloop :: B.Value
> infiniteloop = B.eval omega2

> infiniteloop' :: Term
> infiniteloop' = A.reduce omega2

> data Delay a
>   = Wait (Delay a)   -- Quotient by  Wait x == x
>   | Return a

> newtype Fuel = Fuel Int

> runWith :: Fuel -> Delay a -> Maybe a
> runWith (Fuel 0) (Wait m) = Nothing
> runWith (Fuel n) (Wait m) = runWith (Fuel n) m
> runWith (Fuel n) (Return a) = Just a

> instance Monad Delay where
>   return = Return
>   Return a >>= k = k a
>   Wait m >>= k = Wait (m >>= k)

> instance Functor Delay where fmap = liftM
> instance Applicative Delay where pure = return ; (<*>) = ap

> data Value
>   = BV Bool
>   | FV (Value -> Delay Value)

> evalWith :: (Name -> Value) -> Term -> Delay Value

> evalWith env (Var x) = return (env x)

> evalWith env (App f t) = do
>   fv <- evalWith env f
>   tv <- evalWith env t
>   case fv of
>     FV ff -> ff tv
>     BV bb -> error "Can't apply a boolean"

> evalWith env (Fun x t) = return (FV (\xv -> evalWith (env `setEnv` (x, xv)) t))

>   where
>     setEnv :: (Name -> Value) -> (Name, Value) -> (Name -> Value)
>     setEnv env (x, v) y | x == y = v
>     setEnv env (x, v) y | otherwise = env y

> evalWith env (Con (B b)) = return (BV b)
> evalWith env (Con Not) = return (funBV (\b -> BV (not b)))
> evalWith env (Con And) = return (funBV (\b1 -> funBV (\b2 -> BV (b1 && b2))))
> evalWith env (Con Or ) = return (funBV (\b1 -> funBV (\b2 -> BV (b1 || b2))))

> funBV :: (Bool -> Value) -> Value
> funBV f = FV (\bv -> case bv of BV b -> Return (f b) ; FV g -> error "expected a boolean, not a function")

> emptyEnv :: Name -> Value
> emptyEnv x = error ("Unbound variable: " ++ show x)

> eval :: Term -> Delay Value
> eval = evalWith emptyEnv

> factorialD :: Int -> Delay Int
> factorialD 0 = return 1
> factorialD n = do
>   fn <- Wait (factorialD (n - 1))   -- Recursive call "guarded" by "Wait" prevents infinite loops
>   return (n * fn)

> infiniteWait :: Delay Value
> infiniteWait = eval omega2

> nothing :: Maybe Value
> nothing = runWith (Fuel 9000) infiniteWait

> fixDelay :: (Delay a -> Delay a) -> Delay a
> fixDelay f = f (Wait (fixDelay f)) -- Recursive call "guarded" by "Wait" prevents infinite loops

> fixDelay1 :: ((a -> Delay b) -> (a -> Delay b)) -> (a -> Delay b)
> fixDelay1 f = f (\x -> Wait (fixDelay1 f x)) -- Recursive call "guarded" by "Wait" prevents infinite loops

A "terminating" interpreter like `eval` is what we call a "well-defined semantics" in math.

