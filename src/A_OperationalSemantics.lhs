> {-# LANGUAGE PatternSynonyms #-}
> module A_OperationalSemantics where

> type Name = String   -- Variable name

> data Term
>   = Var Name
>   | Fun Name Term     -- Lambda, anonymous function
>   | App Term Term
>   | Con Constant

> data Constant
>   = B Bool
>   | Not
>   | And
>   | Or

> pattern ConB :: Bool -> Term
> pattern ConB b = Con (B b)

> -- \x -> x
> idT :: Term
> idT = Fun "x" (Var "x")

> -- \x -> \y -> x
> constT :: Term
> constT = Fun "x" (Fun "y" (Var "x"))

> -- \x -> \y -> y
> constidT :: Term
> constidT = Fun "x" (Fun "y" (Var "y"))

= Substitution

`subst x u t`, often denoted `t[u/x]` or `t[x->u]` in papers.

> subst :: Name -> Term -> Term -> Term

`x[u/x] = u`

> subst x u (Var y) | x == y = u

`y[u/x] = y` if `x /= y`

>                     | otherwise = Var y

`(t1 t2)[u/x] = t1[u/x] t2[u/x]`

> subst x u (App t1 t2) = App (subst x u t1) (subst x u t2)

`c[u/x] = c`

> subst x u (Con c) = Con c

`(\y -> t)[u/x] = (\y -> t[u/x])` (bound `y` is different from free `x`)

> subst x u (Fun y t) = Fun y (subst x u t)

= Evaluate terms

Step by step. With this `step` function.

On paper, we prefer to view it as a step *relation*. The following notations are equivalent:

- `t` steps to `u`
- `t -> u`
- In Haskell: `step t = Just u`

> step :: Term -> Maybe Term

In the pure lambda calculus, the only things that step are function applications.
We first try to reduce the function to get a `Fun` (anonymous function) or function constant (`Not`, `And`, `Or`).

> step (App f t) | Just f' <- step f = Just (App f' t)

On paper, this is written as an "inference rule":

      f -> f'
    -----------
    f t -> f' t

Meaning: if `f` steps to `f'`, then `f t` steps to `f' t`.

Then we try to reduce the argument of the function.

> step (App f t) | Just t' <- step t = Just (App f t')

      t -> t'
    -----------
    f t -> f t'

That rule characterizes "eager evaluation", which is what most languages use.
Haskell uses "lazy evaluation", which is more complex to describe, so we will
not do that.

Finally, once we have a function applied to a (reduced) argument,
the next evaluation rule applies, called beta-reduction.

> step (App (Fun x t2) t1) = Just (subst x t1 t2)

To evaluate the application of a lambda `\x -> t2` to an argument `t1`, we will
evaluate the body of the function `t2` after replacing the function parameter
`x` with the argument `t1`.

    ---------------------------
    (\x -> t2) t1  ->  t2[t1/x]

Next are the rules for the function constants: `Not`, `And`, `Or`.

> step (App (Con Not) (ConB b)) = Just (ConB (not b))
> step (App (App (Con And) (ConB b1)) (ConB b2)) = Just (ConB (b1 && b2))
> step (App (App (Con Or ) (ConB b1)) (ConB b2)) = Just (ConB (b1 || b2))

In other cases, there is no step we can take.

> step (App f x) | otherwise = Nothing
> step (Var x) = Nothing
> step (Fun x t) = Nothing
> step (Con c) = Nothing

We reduce a term by taking steps until we can't.

> reduce :: Term -> Term
> reduce t = case step t of
>   Just u -> reduce u
>   Nothing -> t

= References

- Wikipedia: Lambda calculus. https://en.wikipedia.org/wiki/Lambda_calculus
- Stanford Encyclopedia of Philosophy: Lambda calculus. https://plato.stanford.edu/entries/lambda-calculus/
- Stanford CS class notes. Syntax and semantics. https://stanford-cs242.github.io/f19/lectures/01-2-syntax-semantics.html
- Software Foundations Vol. 1. (See section "Inference Rule Notation".) https://softwarefoundations.cis.upenn.edu/lf-current/Imp.html
