> {-# OPTIONS_GHC -Wno-name-shadowing #-}
> module B_DenotationalSemantics where

> import A_OperationalSemantics (Name, Term(..), Constant(..))

> data Value
>   = BV Bool
>   | FV (Value -> Value)

> evalWith :: (Name -> Value) -> Term -> Value

> evalWith env (Var x) = env x

> evalWith env (App f t) = case (evalWith env f, evalWith env t) of
>   (FV ff, tv) -> ff tv
>   (BV bb, tv) -> error "Can't apply a boolean"

> evalWith env (Fun x t) = FV (\xv -> evalWith (env `setEnv` (x, xv)) t)

>   where
>     setEnv :: (Name -> v) -> (Name, v) -> (Name -> v)
>     setEnv env (x, v) y | x == y = v
>     setEnv env (x, v) y | otherwise = env y

> evalWith env (Con (B b)) = BV b
> evalWith env (Con Not) = funBV (\b -> BV (not b))
> evalWith env (Con And) = funBV (\b1 -> funBV (\b2 -> BV (b1 && b2)))
> evalWith env (Con Or ) = funBV (\b1 -> funBV (\b2 -> BV (b1 || b2)))

> funBV :: (Bool -> Value) -> Value
> funBV f = FV (\bv -> case bv of BV b -> f b ; FV g -> error "expected a boolean, not a function")

> emptyEnv :: Name -> Value
> emptyEnv x = error ("Unbound variable: " ++ x)

> eval :: Term -> Value
> eval = evalWith emptyEnv
