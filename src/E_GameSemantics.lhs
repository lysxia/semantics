> {-# LANGUAGE
>       BangPatterns,
>       BlockArguments,
>       DataKinds,
>       EmptyDataDeriving,
>       GADTs,
>       LambdaCase,
>       PolyKinds,
>       TypeFamilies,
>       StandaloneDeriving #-}
> module E_GameSemantics where

> import A_OperationalSemantics (Name, {- Term(..), Constant(..) -})
> import Control.Monad (ap, liftM)
> import Data.Functor ((<&>))
> import GHC.Stack (HasCallStack)
> import Debug.Trace

There are two kinds of moves: questions and answers.
Every answer must answer a previously asked question.

> data Question
>   = Eval
>   deriving (Eq, Ord, Show)

> data Answer
>   = B Bool
>   | Z Integer
>   deriving (Eq, Ord, Show)

Polarities: the game is played on multiple boards, or communication channels,
and on each one, only one player may ask questions, while the other must answer them
(possibly after playing elsewhere for a few turns).

We keep track of who's allowed to do what using an empty and unit type.

> data X     deriving (Eq, Ord, Show)
> data Y = Y deriving (Eq, Ord, Show)

> type P f = f X Y  -- Positive polarity
> type N f = f Y X  -- Negative polarity

A path points to one of the boards. It can be read as a path to a leaf in a
(higher-order) function type. For example, a program of type
`(A -> B) -> (C -> D)` may only ask questions to the environment on `B` and
`C`, and provide answers on `A` and `D`.

The path to A is Arg (Arg (Here Y)) : Path X Y.
The path to B is Arg (Res (Here Y)) : Path Y X.
The path to C is Res (Arg (Here Y)) : Path Y X.
The path to D is Res (Res (Here Y)) : Path X Y.

Note that `(A -> B) -> (C -> D)` is covariant in A and D (associated with `Path X Y`)
while is contravariant in `B` and `C` (associated with `Path Y X`).

> data Path x y = Here y | Arg (Path y x) | Res (Path x y)
>   deriving (Eq, Ord, Show)

Open terms may also play moves "on free variables".

A turnstile path refers to a location in a typing judgement `x : A, y : B |- C`:
either the result `C`, or a variable `x : A` (with inverted polarity).

> data TPath x y
>   = R (Path x y)
>   | V Name (Path y x)
>   deriving (Eq, Ord, Show)

A move is either a question or an answer on a board located by a `TPath`.
The polarity of the board determines which player may ask a question or an answer.

> data QA (isQ :: Bool) where
>   Q :: Question -> QA 'True
>   A :: Answer -> QA 'False

> deriving instance Eq   (QA b)
> deriving instance Ord  (QA b)
> deriving instance Show (QA b)

Every question has a unique identifier `ID` and is to be answered by an answer
with the same `ID`.

> newtype ID = ID Integer
>   deriving (Eq, Ord, Show)

> type family FstOrSwap (b :: Bool) (x :: a) (y :: a) :: a where
>   FstOrSwap 'True x y = x
>   FstOrSwap 'False x y = y

> type ElseSwap b f x y = f (FstOrSwap b y x) (FstOrSwap b x y)

> data Move x y where
>   Move :: QA isQ -> ElseSwap isQ TPath x y -> ID -> Move x y

> instance (Show x, Show y) => Show (Move x y) where
>   showsPrec d (Move q@(Q _) m i) = showsPrec d (q, m, i)
>   showsPrec d (Move a@(A _) m i) = showsPrec d (a, m, i)

Synonyms to save on parentheses.

> type PMove = P Move  -- positive moves
> type NMove = N Move  -- negative moves

Ambient effects: programs may loop forever, which we make observable with
a `Wait` operation; and we will need to generate unique `ID`s with `Fresh`.

> data M a
>   = Pure a
>   | Wait (M a)
>   | Fresh (ID -> M a)

> instance Functor M where fmap = liftM
> instance Applicative M where pure = Pure ; (<*>) = ap
> instance Monad M where
>   Pure a >>= k = k a
>   Wait m >>= k = Wait (m >>= k)
>   Fresh f >>= k = Fresh ((>>= k) . f)

A strategy for the first player.

A strategy consists of an initial move, and then responds a move to every comove.
Strategies are partial: they may not have an answer to every move (some moves
are completely nonsensical, others will be handled by composing with other
strategies).

This is a profunctor (hence the order of arguments).

> data Strategy comove move = Strategy
>   { move :: move
>   , coplay :: comove -> Maybe (M (Strategy comove move))
>   }

A costrategy (or counter-strategy) is a "strategy" for the second player:
they know what move to play after the first player has made a move.

> type CoStrategy comove move = comove -> Maybe (M (Strategy comove move))

(See also `Mealy` and `Moore` in [machines][].)

[machines]: https://hackage.haskell.org/package/machines

Programs as strategies.

> type PStrategy = Strategy NMove PMove
> type PCoStrategy = CoStrategy NMove PMove

Profunctorial maps.

> dimapCS :: (c -> Maybe a) -> (b -> d) -> CoStrategy a b -> CoStrategy c d
> dimapCS ca bd s c = (fmap . fmap) (dimapS ca bd) (ca c >>= s)

> dimapS :: (c -> Maybe a) -> (b -> d) -> Strategy a b -> Strategy c d
> dimapS ca bd (Strategy b s) = Strategy (bd b) (dimapCS ca bd s)

Boolean strategy: semantics of `True` and `False`.
Respond to every `Eval` question with the given `b :: Bool`.

> boolS :: Bool -> PCoStrategy
> boolS b (Move (Q Eval) p i) =
>   assert (p == R (Here Y)) $ Just $ Pure $
>   Strategy (Move (A (B b)) p i) (boolS b)
> boolS _ _ = Nothing

Product of costrategies: if the first strategy rejects a move, then play the
second strategy. Keep going recursively.

> (*$) :: CoStrategy a b -> CoStrategy a b -> CoStrategy a b
> (s1 *$ s2) a | Just z1 <- s1 a = Just (z1 <&> \(Strategy b s1') -> Strategy b (s1' *$ s2))
> (s1 *$ s2) a | Just z2 <- s2 a = Just (z2 <&> \(Strategy b s2') -> Strategy b (s1 *$ s2'))
> (s1 *$ s2) a | otherwise = Nothing

> spawn :: CoStrategy a b -> CoStrategy a b
> spawn s a = s a <&> fmap \(Strategy b s') -> Strategy b (s' *$ spawn s)
> -- TODO: this leaks space

Copycat strategy, semantics of `id : A -> A`.

> invert :: Path x y -> Maybe (Path y x)
> invert (Res p) = Just (Arg p)
> invert (Arg p) = Just (Res p)
> invert (Here _) = Nothing

> idS :: PCoStrategy
> idS = spawn \case
>   Move (Q q) (R p) i ->
>     invert p <&> \p' ->
>     Fresh \i' ->
>     Pure $ Strategy (Move (Q q) (R p') i') \case
>       Move (A a) (R p'') i'' | i' == i'' ->
>         assert (p' == p'') $ Just $
>         Pure $ Strategy (Move (A a) (R p) i) \_ -> Nothing
>       _ -> Nothing
>   _ -> Nothing

> invertT :: Name -> TPath x y -> Maybe (TPath y x)
> invertT x (R p) = Just (V x p)
> invertT x (V x' p) | x == x' = Just (R p)
> invertT x _ = Nothing

Identity typing rule `x : A |- x : A`. Semantics of `x : A` in context `x : A`.

> varS :: Name -> PCoStrategy
> varS x = spawn \case
>   Move (Q q) p i ->
>     invertT x p <&> \p' ->
>     Fresh \i' ->
>     Pure $ Strategy (Move (Q q) p' i') \case
>       Move (A a) p'' i'' | i' == i'' ->
>         assert (p' == p'') $ Just $
>         Pure $ Strategy (Move (A a) p i) \_ -> Nothing
>       _ -> Nothing
>   _ -> Nothing

> funS :: Name -> PCoStrategy -> PCoStrategy
> funS x = dimapCS toVar fromVar where
>
>   toVar :: NMove -> Maybe NMove
>   toVar (Move m (R (Res p)) i) = Just (Move m (R p) i)
>   toVar (Move m (R (Arg p)) i) = Just (Move m (V x p) i)
>   toVar (Move m (V x' p) i) | x == x' = Nothing  -- Shadowed
>   toVar m = Just m
>
>   fromVar :: PMove -> PMove
>   fromVar (Move m (R p) i) = Move m (R (Res p)) i
>   fromVar (Move m (V x' p) i) | x == x' = Move m (R (Arg p)) i
>   fromVar m = m

`idS = funS x (varS x)`
`id = \x -> x`

(Better) Product of strategies

> data These a b
>   = This a
>   | That b
>   | These a b

> this :: These a b -> Maybe a
> this (This a) = Just a
> this (These a b) = Just a
> this (That b) = Nothing

> that :: These a b -> Maybe b
> that (This a) = Nothing
> that (These a b) = Just b
> that (That b) = Just b

> prod :: (a -> These b c) -> (Either d e -> Either (These b c) f) -> CoStrategy b d -> CoStrategy c e -> CoStrategy a f
> prod abc def s1_ s2_ = (s1_ ?? s2_) . abc where
>
>   (s1 ?? s2) bc | Just b <- this bc, Just z1 <- s1 b
>     = Just $ z1 >>= \(Strategy d s1') -> (s1' ??? s2) (def (Left d))
>   (s1 ?? s2) bc | Just c <- that bc, Just z2 <- s2 c
>     = Just $ z2 >>= \(Strategy d s2') -> (s1 ??? s2') (def (Right d))
>   (s1 ?? s2) bc | otherwise = Nothing
>
>   (s1 ??? s2) (Left bc)
>     | Just s <- (s1 ?? s2) bc = Wait s
>     | otherwise = error "Incomplete strategy"
>   (s1 ??? s2) (Right f) = Pure (Strategy f ((s1 ?? s2) . abc))

```
Γ ⊢ N : A -> B    Γ ⊢ M : A
--------------------------- (app)
       Γ ⊢ N M : B
```

1. If `N M` receives a move on `B`, forward it to `N : A -> B`.
2. If `N M` receives a move on `Γ` (from the environment),
   forward it to both `N` and `M` (exactly one of them is expecting that move).
3. If `N` sends a move on `A`, forward it to `M : A`.
4. If `N` sends a move on `B` or `Γ`, send it to out to the environment.
5. If `M` sends a move on `A`, forward it to `N : A -> B`.
6. If `N` sends a move on `Γ`, send it out to the environment.

Those cases correspond to the cases numbered by a comment below:

> infixl 6 `appS`
> appS :: PCoStrategy -> PCoStrategy -> PCoStrategy
> appS = prod incoming outgoing where
>
>   incoming :: NMove -> These NMove NMove
>   incoming (Move m (R b) i) = This (Move m (R (Res b)) i)                   -- (1)
>   incoming mm@(Move m (V x p) i) = These mm mm                              -- (2)
>
>   outgoing :: Either PMove PMove -> Either (These NMove NMove) PMove
>   outgoing (Left (Move m (R (Res pb)) i)) = Right (Move m (R pb) i)         -- (3)
>   outgoing (Left (Move m (R (Arg pa)) i)) = Left (That (Move m (R pa) i))   -- (4) on B
>   outgoing (Left (Move m (R (Here _)) i)) = error "unexpected Here move"
>   outgoing (Left mm@(Move m (V x p) i)) = Right mm                          -- (4) on Γ
>   outgoing (Right (Move m (R pa) i)) = Left (This (Move m (R (Arg pa)) i))  -- (5)
>   outgoing (Right mm@(Move m (V x p) i)) = Right mm                         -- (6)

> assert :: HasCallStack => Bool -> a -> a
> assert True x = x
> assert False _ = error "Assertion failed"

The Y combinator.

> -- fix = (\ω f -> f (ω ω f)) (\ω f -> f (ω ω f))
> fixS :: PCoStrategy
> fixS = appS omegaS omegaS
>
> -- omega = (\ω f -> f (ω ω f))
> omegaS :: PCoStrategy
> omegaS = funS "ω" (funS "f" (varS "f" `appS` (varS "ω" `appS` varS "ω" `appS` varS "f")))

> -- factorial = fix \factorial n ->
> --   if n == 0 then 1 else n * factorial (n - 1)
> factorialS :: PCoStrategy
> factorialS = appS fixS $ funS "factorial" $ traceS "1" $ funS "n" $
>   traceS "2" ifS `appS` (eqS `appS` varS "n" `appS` intS 0)
>       `appS` (intS 1)
>       `appS` (timesS `appS` varS "n"
>                      `appS` (varS "factorial" `appS` (minusS `appS` varS "n" `appS` intS 1)))

> plusS :: PCoStrategy
> plusS = binopS (arith (+))

> minusS :: PCoStrategy
> minusS = binopS (arith (-))

> timesS :: PCoStrategy
> timesS = binopS (arith (*))

> -- eqS :: Int -> Int -> Bool
> eqS :: PCoStrategy
> eqS = binopS (cmp (==))

> cmp :: (Integer -> Integer -> Bool) -> Answer -> Answer -> Maybe Answer
> cmp f (Z n1) (Z n2) = Just (B (f n1 n2))
> cmp _ _ _ = Nothing

> arith :: (Integer -> Integer -> Integer) -> Answer -> Answer -> Maybe Answer
> arith f (Z n1) (Z n2) = Just (Z (f n1 n2))
> arith _ _ _ = Nothing

> binopS :: (Answer -> Answer -> Maybe Answer) -> PCoStrategy
> binopS (.+) = spawn \case
>   Move (Q Eval) p i ->
>     assert (p == R (Res (Res (Here Y)))) $ Just $
>     Fresh \j1 ->
>     Pure $ Strategy (Move (Q Eval) (R (Arg (Here Y))) j1) \case
>       Move (A a1) (R (Arg (Here Y))) j1' | j1 == j1' -> Just $
>         Fresh \j2 ->
>         Pure $ Strategy (Move (Q Eval) (R (Res (Arg (Here Y)))) j2) \case
>           Move (A a2) (R (Res (Arg (Here Y)))) j2' | j2 == j2' -> Just $
>             case a1 .+ a2 of
>               Just a3 -> Pure $ Strategy (Move (A a3) p i) \_ -> Nothing
>               Nothing -> error "Type error"
>           _ -> Nothing
>       _ -> Nothing
>   _ -> Nothing

> intS :: Integer -> PCoStrategy
> intS n (Move (Q Eval) p@(R (Here Y)) i) = Just $
>     Pure $ Strategy (Move (A (Z n)) p i) (intS n)
> intS _ _ = Nothing

> -- const = \x y -> x
> constS :: PCoStrategy
> constS = funS "x" $ funS "y" $ varS "x"

> -- if :: Bool -> a -> a -> a
> ifS :: PCoStrategy
> ifS = spawn \case
>   m@(Move (Q q) (R (Res (Res (Res p)))) i) -> Just $
>     Fresh \j ->
>     Pure $ Strategy (Move (Q Eval) (R (Arg (Here Y))) j) \case
>       Move (A (B b)) p j' | j == j' ->
>         assert (p == R (Arg (Here Y))) $
>         if b then (constS `appS` constS) m else (constS `appS` (constS `appS` idS)) m  -- TODO: this should reject moves on Bool; don't use const
>       _ -> Nothing
>   _ -> Nothing

> traceS :: String -> PCoStrategy -> PCoStrategy
> traceS pref s m =
>   case s m of
>     Nothing -> Nothing
>     Just z -> Just $
>       trace (pref ++ "< " ++ show m) $
>       z <&> \(Strategy m' s') ->
>         trace (pref ++ "> " ++ show m') $ Strategy m' (traceS pref s')

> runM :: M a -> a
> runM = go 0 where
>   go !i (Pure a) = a
>   go i (Fresh f) = go (i+1) (f (ID i))
>   go i (Wait m) = go i m

Run a closed strategy of ground type (int or bool)

> runCS :: PCoStrategy -> Answer
> runCS s = runM $ Fresh \i ->
>   case s (Move (Q Eval) (R (Here Y)) i) of
>     Nothing -> error "bad question"
>     Just z -> z >>= \case
>       Strategy (Move (A a) _ _) _ -> Pure a
>       _ -> error "bad answer"

> assertIO :: HasCallStack => Bool -> IO ()
> assertIO b = assert b (pure ())

> test :: IO ()
> test = do
>   assertIO $ runCS (boolS True) == B True
>   assertIO $ runCS (boolS False) == B False
>   assertIO $ runCS (appS idS (boolS True)) == B True
>   assertIO $ runCS (appS (funS "x" (varS "x")) (boolS True)) == B True
>   assertIO $ runCS (factorialS `appS` intS 3) == Z 6
